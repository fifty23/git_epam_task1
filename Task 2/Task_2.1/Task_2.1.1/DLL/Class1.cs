﻿using System;

namespace DLL
{
    public static class CustomString
    {
        public static bool Equality(char[] str1, char[] str2)
        {
            if (str1.Length != str2.Length)
            {
                return false;
            }
            for (int i = 0; i < str1.Length; i++)
            {
                if (str1[i] != str2[i])
                {
                    return false;
                }
            }
            return true;
        }

        public static char[] Concatination(char[] str1, char[] str2)
        {
            char[] finalString = new char[str1.Length + str2.Length];
            for (int i = 0; i < str1.Length; i++)
            {
                finalString[i] += str1[i];

            }
            for (int k = 0; k < str2.Length; k++)
            {
                finalString[k + str1.Length] += str2[k];
            }
            return finalString;
        }

        public static bool Search(char[] str1, char key)
        {
            foreach (var sym in str1)
            {
                if (sym == key)
                {
                    return true;
                }
            }
            return false;
        }

        public static void Print(char[] str)
        {
            foreach (var letter in str)
            {
                Console.Write(letter);
            }
        }

        public static char[] ToArray(char[] str)
        {
            return str;
        }

        public static char Indexer(char[] str1, int index)
        {
            return str1[index];
        }
    }
}
