﻿using System;

namespace Task_1._1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число строк:");
            int.TryParse(Console.ReadLine(), out int N);

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < i + 1; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
