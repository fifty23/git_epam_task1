﻿using System;

namespace Task_1._1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("введите число строк:");
            int.TryParse(Console.ReadLine(), out int N);
            for (int i = 0; i < N; i++)
            {

                for (int j = i; j < N - 1; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < i; k++)
                {
                    Console.Write("**");
                }
                Console.WriteLine("*");
            }
        }
    }
}
