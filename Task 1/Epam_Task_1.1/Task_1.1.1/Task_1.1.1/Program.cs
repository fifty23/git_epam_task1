﻿using System; 

namespace Task_1._1._1
{
    class Program
    {
     
        static void Main(string[] args)
        {
            Console.Write("Введите значение a:");
            int.TryParse(Console.ReadLine(), out int a);
           
            Console.Write("Введите значение b:");
            int.TryParse(Console.ReadLine(), out int b);


            if (a <= 0 || b <= 0)
            {
                Console.WriteLine("Введите положительное число");
                return; 
            }

            int c = a * b;

            Console.WriteLine($"Площадь треугольника равна {c}");


        }
    }
}
